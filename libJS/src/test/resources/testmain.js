requirejs.config({
    // path where all main libs are located, e.g. kotlin.js
    baseUrl: '/base/libJS/build/lib',

    paths: { // paths are relative to this file
        'libJS': '../../build/classes/kotlin/main/libsJS',
        'libJS_test': '../../build/classes/kotlin/test/libJS_test',
    },

    deps: ['libJS_test'],

    // start tests when done
    callback: window.__karma__.start
});
