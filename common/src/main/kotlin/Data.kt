package test

import kotlinx.serialization.Serializable


@Serializable
class Data(val a: Int)
