package crosstest

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JSON
import test.Data
import kotlin.test.Test


@Serializable
class LocalBox(val value: Data)

class CrossModuleTest {
    @Test
    fun testBox() {
        val obj = LocalBox(value = Data(5))
        val testJson = JSON.stringify(obj)
        println(testJson)
        val test = JSON.parse<LocalBox>(testJson)
        println(test)
    }
}